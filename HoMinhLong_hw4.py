#1
#print 'Hello, World!' 
#Output : Hello , World!
#Ở python 2. câu lệnh print có thể bỏ trong ngoặc đơn hoặc không cần .
print('Hello ,World!')
#Ở python 3. câu lệnh print bắt buột phải bỏ trong ngoặc đơn 
#Mục đích là ở python 2. việc không bỏ vào ngoặc đơn có thể gây ra output không đúng mong đợi , nếu truyền vào nhiều tham số nó sẽ tạo ra 1 bộ
#Do đó khi ta chạy lệnh print không có ngoặc đơn ở Python 3. sẽ báo lỗi Syntax Error .

#2
#print '1/2'
#Ở python 2. câu lệnh sẽ cho ra Output là 1
Print ('1/2')
#Ở python 3. câu lệnh sẽ cho ra Output là 0.5

#3
#type(1/2)
#Do sự khác biệt ở 2 version được trình bày ở #2
#Ta dễ dàng nhận thấy Output ở version 2 cho ra là 1 và version 3 là 0.5
#Nên khi ta thực thi type ở 2 phiên bản sẽ có Output là int và ở python 3 là float

#4 
#print(01)
#Output py2 : 1
#Output py3: SyntasError báo lỗi không được phép để số 0 phía trước 
#trừ 1 số trường hợp như là số thập phân vd 0.3 , hoặc 00 sẽ cho ra output là 0

#5
#1/(2/3)
#Do sự khác biệt ở 2 version được trình bày ở #2
#Nên Output của 2 version sẽ đưa ra kết quả khác nhau
#Output py2 : 2/3 = 0.6..  chương trình mặc định sẽ hiểu là = 0 chứ không phải 0.6 nên khi 1/0 sẽ báo lỗi Division Zero
#Output py3 : 1/(2/3) tức là = 1*3/2 nên output sẽ là 1.5
